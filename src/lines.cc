#include <simplex.h>

#include <unistd.h>
#include <sys/time.h>

int main() {
    xwin_t   win;
    pixbuf_t pb;

    // Create window, set title
    xwin_init (&win, 0, 800, 600, -1, -1);
    xwin_title(&win, "Line Drawing Demo");

    // Create initial pixbuf for the window
    pixbuf_init  (&pb,     800, 600);
    pixbuf_redraw(&pb, rectangle(0, 0, 800, 600));

    // Event loop
    bool running=true;
    while(running) {
        event_t evt;
        if (xwin_event(&win, &evt)) {
            switch(evt.type) {
            case WINDOW_RESIZE: {
                pixbuf_resize(&pb, evt.w, evt.h); // Resize pixbuf to match window
                pixbuf_clear (&pb, rgb_color(0x00000A));

                pixel_t color = {255, 255, 255, 255};
                for (double ii=0; ii < 360; ii += 7.5) {
                    draw_linef(&pb, color, 200, 200, 200+150*cos(ii*M_PI/180.0), 200+150*sin(ii*M_PI/180.0), 1);
                }

                for (double ii=0; ii < 360; ii += 7.5) {
                    draw_line(&pb, color, 600, 200, 600+150*cos(ii*M_PI/180.0), 200+150*sin(ii*M_PI/180.0));
                }

                for (ssize_t ii=0; ii < 10; ii++) {
                    draw_linef(&pb, color, 5+79*ii,400, 5+79*ii+50, 500, 0.01+ii);
                }
                
                pixbuf_redraw(&pb, rectangle(0, 0, evt.w, evt.h));
                
                break;
            }

            case MOUSE_DOWN:
                break;

            case MOUSE_UP:                
                break;

            case MOUSE_DRAG:
            case MOUSE_MOVE:
                break;            

            case WINDOW_CLOSE:
                running=false;
            case KEY_PRESS:
                running=false;
                break;
            default:
                break;
            }
        } else {
            usleep(1000);
        }

        xwin_composite(&win, &pb);
    }

    return 0;
}
