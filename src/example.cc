/*******************************************************************************
 * Copyright (c) 2012 Sean McAllister
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:irec
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/

#include <simplex.h>

#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>

typedef struct {
    xwin_t   win;
    pixbuf_t pb;     // main rendering surface
    pixbuf_t cursor; // pixbuf to hold cursor and mouse text
    
    int mousex;
    int mousey;
    int mousedown;

    int hotitem;
    int activeitem;

    int mousedrag;
    int dragw, dragh;
    
    pixel_t plotcolor;
} ui_t;

const static int default_miny = font_miny(font_default);

// Draw a button of the given size with the given text
void button(ui_t *ui, uint16_t x0, uint16_t y0, uint16_t ww, uint16_t hh, const char* label, bool active) {
    pixel_t scolor = { 64, 64, 64, 255};
    pixel_t ecolor = {  0,  0,  0, 255};

    if (active) {
        ecolor = rgb_pixel(64, 64, 64);
        scolor = rgb_pixel(0,   0,  0);
    }
     
    // Draw gradient box
    for (size_t yy=y0; yy < y0+hh; yy++) {
        pixel_t color = { (uint8_t)(scolor.r + ((float)ecolor.r-scolor.r)*(yy-y0)/hh), 
                          (uint8_t)(scolor.g + ((float)ecolor.g-scolor.g)*(yy-y0)/hh), 
                          (uint8_t)(scolor.b + ((float)ecolor.b-scolor.b)*(yy-y0)/hh), 
                          (uint8_t)(scolor.a + ((float)ecolor.a-scolor.a)*(yy-y0)/hh)};

        for (size_t xx=x0; xx < x0+ww; xx++) {
            draw_pixel(&ui->pb, color, xx, yy);
        }
    }


    pixel_t color = { 128, 128, 128, 255};
    uint16_t tw,th; text_bbox(label, &tw, &th);
    draw_text(&ui->pb, color, x0+ww/2-tw/2, y0+hh+default_miny-2, label);    
    
    pixbuf_redraw(&ui->pb, rectangle(x0, y0, ww, hh));
}


// Draw an info box with given info string
void info_box(ui_t *ui, uint16_t x0, uint16_t y0, uint16_t ww, uint16_t hh, const char *info) {
    // Draw outline
    pixel_t color = {128, 128, 128, 255};
    draw_circ (&ui->pb, color, x0,    y0,       10, IM_LEFT_HALF);
    draw_hline(&ui->pb, color, x0+10, x0+ww-12, y0,    1);
    draw_hline(&ui->pb, color, x0+10, x0+ww-12, y0+hh, 1);
    draw_circ (&ui->pb, color, x0+ww-22, y0, 10, IM_RIGHT_HALF);

    // Draw text
    color = rgb_pixel(200,200,200);
    uint16_t tw,th; text_bbox(info, &tw, &th);
    draw_text(&ui->pb, color, x0+ww/2-tw/2, y0+hh+default_miny-2, info);    
}


// Draw a simple dummy plot using noise
void draw_noise(ui_t *ui, unsigned seed, uint16_t x0, uint16_t y0, uint16_t ww, uint16_t hh) {
    srand(seed);
    
    pixel_t color = {0,0,0,255};
    pixbuf_clear_rect(&ui->pb, color, rectangle(x0, y0, ww, hh));

    draw_rect(&ui->pb, rgb_pixel(255, 255, 255), rectangle(x0, y0, ww, hh), 1);

    for (uint16_t xx=x0+1; xx < x0+ww-1; xx++) {
        uint16_t height = (float)rand() * hh / (float)RAND_MAX / 1.2;        
        draw_vline(&ui->pb, ui->plotcolor, xx, y0+hh-2, y0+hh-height-2, 1);
    }

    pixbuf_redraw(&ui->pb, rectangle(x0, y0, ww, hh));
}


/********************************************************************************
 * Draw a custom cursor and a little box that follows it around displaying the
 * mouse coordinates as an example of a transparent overlay
 ********************************************************************************/
void redraw_cursor(ui_t *ui, pixbuf_t *pb, uint16_t x, uint16_t y, uint16_t frames) {
    const size_t width=130;

    memset(pb->pixels, 0, pb->width*pb->height*sizeof(pixel_t));

    if ((pb->width != width+35) || (pb->height != 26)) {
        pixbuf_resize(pb, width+35, 26);
    }
    
    // Draw background with rounded corners
    pixel_t color = rgba_pixel(128, 0, 0, 128);
    draw_circ_fill(pb, color, 5,  5, 10,  IM_LEFT_HALF);
    draw_rect_fill(pb, color, rectangle(16, 5, width,  21));
    draw_circ_fill(pb, color, 5+width+1, 5, 10,  IM_RIGHT_HALF);
    
    // Generate label containing mouse coordinates
    char msg[128];  sprintf(msg, "(%i, %i) - %i", ui->mousex, ui->mousey, frames);
    uint16_t ww,hh; text_bbox(msg, &ww, &hh);

    // Set the color to white and render mouse coordinates
    color = rgba_pixel(255, 255, 255, 255);
    draw_text(pb, color, pb->width/2-ww/2, pb->height/2+hh+default_miny-3, msg);

    // Draw cursor, do this rather than relying on X11 cursor that can get out of sync with our graphics
    draw_vline(pb, color,  5,  0, 10, 1);
    draw_hline(pb, color,  0, 10,  5, 1);

    // Move the layer to align with mouse pointer
    uint16_t oldx = pb->x, oldy = pb->y;
    pb->x = x;
    pb->y = y;

    // redraw old area and new area to clear old pixels
    pixbuf_redraw(&ui->pb, rect_bbox(rectangle(oldx, oldy, ui->cursor.width, ui->cursor.height),
                                     rectangle(   x,    y, ui->cursor.width, ui->cursor.height)));
}


// Draws two text areas with mouse coordinates inside them
void draw_mouse_labels(ui_t *ui) {
    pixel_t color = {0,0,0,255};    
    draw_rect_fill(&ui->pb, color, rectangle(600, 20, 180, 60));

    // Draw two labels to show mouse position
    char label[128];    
    sprintf(label, "%i", ui->mousex); info_box(ui, 680,  20, 100, 20, label);
    sprintf(label, "%i", ui->mousey); info_box(ui, 680,  50, 100, 20, label);
    
    color = rgba_pixel(128, 0, 0, 255);
    draw_text(&ui->pb, color, 600, 40+default_miny-2, "X Position");
    draw_text(&ui->pb, color, 600, 70+default_miny-2, "Y Position");
   
    pixbuf_redraw(&ui->pb, rectangle(600, 20, 180, 60));
}


// Draws two text areas with drag distance inside them when dragging
void draw_drag_labels(ui_t *ui) {
    pixel_t color = {0,0,0,255};
    draw_rect_fill(&ui->pb, color, rectangle(600, 80, 180, 60));
    
    char label[128];    
    sprintf(label, "%i", ui->dragw); info_box(ui, 680,  80, 100, 20, label);
    sprintf(label, "%i", ui->dragh); info_box(ui, 680, 110, 100, 20, label);
    
    color = rgba_pixel(128, 0, 0, 255);
    draw_text(&ui->pb, color, 600, 100+default_miny-2, "X Drag");
    draw_text(&ui->pb, color, 600, 130+default_miny-2, "Y Drag");    

    pixbuf_redraw(&ui->pb, rectangle(600, 80, 180, 60));
}

void draw_fps(ui_t *ui, float fps) {
    // display FPS
    draw_rect_fill(&ui->pb, rgb_pixel(0,0,0), rectangle(600, 140, 180, 25));
    char label[256]; sprintf(label, "%.1f", fps); info_box(ui, 680, 140, 100, 20, label);
    
    draw_text    (&ui->pb, rgb_pixel(128, 0, 0), 600, 160+default_miny-2, "FPS");
    pixbuf_redraw(&ui->pb, rectangle(600, 140, 180, 25));
}


int main() {
    ui_t ui; memset(&ui, 0, sizeof(ui_t));
    ui.plotcolor = rgba_pixel(128, 0, 128, 255);    
    
    // Create window, set title, disable cursor
         xwin_init(&ui.win, 0, 800, 600, -1, -1);
        xwin_title(&ui.win, "GUI Demo");
    xwin_no_cursor(&ui.win);

    // Create initial pixbuf for the window
    pixbuf_init    (&ui.pb,     800, 600);
    pixbuf_init    (&ui.cursor, 120, 26);   

    ui.cursor.x = ui.cursor.y = 0;
    pixbuf_add_child(&ui.pb, &ui.cursor);
    
    // Create buttons
    rect_t button_1 = rectangle(20,  400, 100, 20);
    rect_t button_2 = rectangle(130, 400, 100, 20);

    // Event loop
    bool     running=true;
    timeval  ctime, ltime={0,0};
    uint16_t frames=0;
    float    fps   =0.0;
    unsigned seed  =0;
    while(running) {
        event_t evt;
        if (xwin_event(&ui.win, &evt)) {
            switch(evt.type) {
            case WINDOW_RESIZE: {
                pixbuf_resize(&ui.pb, evt.w, evt.h); // Resize pixbuf to match window
                pixbuf_redraw(&ui.pb, rectangle(0, 0, evt.w, evt.h));
                
                // Redraw standard stuff
                draw_mouse_labels(&ui);
                draw_noise(&ui, seed, 20, 20, 550, 350);
                draw_fps  (&ui, fps);
                
                // Define button area and draw the initial buttons
                button(&ui, button_1.x0, button_1.y0, rect_width(button_1), rect_height(button_1), "Color",   false);
                button(&ui, button_2.x0, button_2.y0, rect_width(button_2), rect_height(button_2), "Message", false);    

                break;
            }

            case MOUSE_DOWN:
                if (evt.m == 1) {
                    ui.mousedown = 1;

                    if (point_in_rect(evt.x, evt.y, button_1)) {
                        button(&ui, button_1.x0, button_1.y0, rect_width(button_1), rect_height(button_1), "Color",   true);
                    }

                    if (point_in_rect(evt.x, evt.y, button_2)) {
                        button(&ui, button_2.x0, button_2.y0, rect_width(button_2), rect_height(button_2), "Message", true);
                    }
                }
                break;

            case MOUSE_UP:
                ui.mousedown = 0;
                ui.mousedrag = 0;
                ui.dragw = ui.dragh = 0;

                {
                    // Clear drag labels
                    pixel_t color = {0,0,0,255};
                    draw_rect_fill(&ui.pb, color, rectangle(600, 80, 180, 60));
                    pixbuf_redraw(&ui.pb, rectangle(600, 80, 180, 60));
                }

                if (point_in_rect(evt.x, evt.y, button_1)) {
                    ui.plotcolor = rgba_pixel(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF, 255);
                }

                if (point_in_rect(evt.x, evt.y, button_2)) {
                    printf("Button clicked!\n");
                }

                button(&ui, button_1.x0, button_1.y0, rect_width(button_1), rect_height(button_1), "Color",   false);
                button(&ui, button_2.x0, button_2.y0, rect_width(button_2), rect_height(button_2), "Message", false);
                
                break;

            case MOUSE_DRAG:
                ui.mousedrag = 1;
                ui.dragw     = evt.w;
                ui.dragh     = evt.h;

                button(&ui, button_1.x0, button_1.y0, rect_width(button_1), rect_height(button_1), "Color",
                       point_in_rect(evt.x, evt.y, button_1));
                button(&ui, button_2.x0, button_2.y0, rect_width(button_2), rect_height(button_2), "Message",
                       point_in_rect(evt.x, evt.y, button_2));


            case MOUSE_MOVE:
                ui.mousex    = evt.x;
                ui.mousey    = evt.y;
                if ((evt.x >= 0) && (evt.y >= 0)) {
                    redraw_cursor(&ui, &ui.cursor, evt.x, evt.y, frames);
                }
               
                if (ui.mousedrag) {
                    draw_drag_labels(&ui);
                }                
                
                draw_mouse_labels(&ui);
                
                break;            
            case WINDOW_CLOSE:
                running=false;
            case KEY_PRESS:
                running=false;
                break;
            default:
                break;
            }
        } else {
            usleep(1000);
        }

        // Check for time elapsed before re-rendering to limit framerate
        gettimeofday(&ctime, NULL);
        double etime = (double)(ctime.tv_sec-ltime.tv_sec) + (double)(ctime.tv_usec - ltime.tv_usec)/1e6;

        // target 20FPS
        if (etime >= 1/20.0) {
            fps  = 1.0/etime;
            seed = rand();

            draw_fps  (&ui, fps);
            draw_noise(&ui, seed, 20, 20, 550, 350);
            ltime = ctime;

            // redraw cursor
            frames++;            
            redraw_cursor(&ui, &ui.cursor, ui.mousex, ui.mousey, frames);
        }

        xwin_composite(&ui.win, &ui.pb);
    }

    return 0;              
}
