#include <simplex.h>

#include <time.h>
#include <unistd.h>
#include <sys/time.h>

typedef struct {
    float   x, y;
    float   dx,dy;
    int     radius;
    pixel_t color;
    float   lx,ly;  // lastx, lasty
} ball_t;

ball_t* create_balls(size_t nball, size_t width, size_t height) {
    srand(time(NULL));
    
    ball_t *balls = (ball_t*)malloc(nball*sizeof(ball_t));
    for (size_t ii=0; ii < nball; ii++) {
        balls[ii].x      =  rand() % width;
        balls[ii].y      =  rand() % height;
        balls[ii].dx     = (rand() % 3000) / 10.0 - 150;
        balls[ii].dy     = (rand() % 3000) / 10.0 - 150;
        balls[ii].color  = rgba_pixel(    255*ii/(float)nball,
                                      255-255*ii/(float)nball,
                                  (int)(4*255*ii/(float)nball) % 255,
                                          ((rand() & 0x1) == 0) ? 128 : 255);
        balls[ii].radius = rand() % (10-2) + 2;
        
        // no last x/y to clear
        balls[ii].lx     = -1;
        balls[ii].ly     = -1;
    }
    return balls;
}


void update_balls(ball_t *balls, size_t nball, double dt, int width, int height) {
    for (size_t ii=0; ii < nball; ii++) {
        //printf("dt: %f  dx: %f  dy: %f\n", dt, balls[ii].dx, balls[ii].dy);
        balls[ii].x += dt*balls[ii].dx;
        balls[ii].y += dt*balls[ii].dy;

        //printf("x: %f  y: %f\n", balls[ii].x, balls[ii].y);

        int radius = balls[ii].radius;
        if (balls[ii].x+2*radius >= width) {
            balls[ii].x  = width-2*radius-1;;
            balls[ii].dx = -balls[ii].dx;
        }

        if (balls[ii].x <  0) {
            balls[ii].x  = 0;
            balls[ii].dx = -balls[ii].dx;
        }

        if (balls[ii].y+2*radius >= height) {
            balls[ii].y  = height-2*radius-1;
            balls[ii].dy = -balls[ii].dy;
        }

        if (balls[ii].y <  0) {
            balls[ii].y  = 0;
            balls[ii].dy = -balls[ii].dy;
        }
    }
}


void redraw_balls(pixbuf_t *pb, ball_t *balls, size_t nball) {
    // clear old balls
    for (size_t ii=0; ii < nball; ii++) {
        ball_t ball   = balls[ii];
        int    radius = ball.radius;
        if (ball.lx >= 0 && ball.ly >= 0) {
            rect_t brect  = rectangle(round(ball.lx)-1, round(ball.ly)-1, 2*radius+3, 2*radius+3);
            draw_rect_fill(pb, rgb_color(0x000000), brect);
            pixbuf_redraw (pb,                      brect);
        }
    }

    for (size_t ii=0; ii < nball; ii++) {
        ball_t ball   = balls[ii];
        int    radius = ball.radius;
        rect_t brect  = rectangle(round(ball.x)-1, round(ball.y)-1, 2*radius+3, 2*radius+3);
        draw_circ_fill(pb, ball.color, round(ball.x), round(ball.y), ball.radius, IM_FULL_CIRCLE);
        pixbuf_redraw (pb, brect);

        balls[ii].lx = ball.x;
        balls[ii].ly = ball.y;
    }
}

void redraw_screen(pixbuf_t *pb, ball_t *balls, size_t nball, float fps) {
    // draw frame rate
    char txt[256];
    sprintf(txt, "Frame Rate: %.1f", fps);
    
    uint16_t ww,hh;
    text_bbox(txt, &ww, &hh);
    draw_rect_fill(pb, rgb_color(0x000000), rectangle(5, 5, ww, hh));
    
    // redraw balls
    redraw_balls(pb, balls, nball);
    
    // draw frame rate
    draw_text     (pb, rgb_color(0xFFFFFF), 5, 5, txt);
    pixbuf_redraw (pb, rectangle(5, 5, ww, hh));
}


int main() {
    xwin_t   win;
    pixbuf_t pb;

    size_t nball =400;
    size_t width =800;
    size_t height=600;

    // Create window, set title
    xwin_init (&win, 0, width, height, -1, -1);
    xwin_title(&win, "Physics/Animation");

    // Create initial pixbuf for the window
    pixbuf_init  (&pb,                 width, height);
    pixbuf_redraw(&pb, rectangle(0, 0, width, height));

    // Create random balls
    ball_t *balls = create_balls(nball, width, height);
    redraw_balls(&pb, balls, nball);
    
    // Event loop
    bool    running = true;
    timeval ctime, ltime;    gettimeofday(&ltime, NULL);
    float   fps=0.0;
    while (running) {
        event_t evt;
        if (xwin_event(&win, &evt)) {
            switch(evt.type) {
            case WINDOW_RESIZE: {
                width  = evt.w;
                height = evt.h;

                pixbuf_resize(&pb, evt.w, evt.h);
                redraw_screen(&pb, balls, nball, fps);
                pixbuf_redraw(&pb, rectangle(0, 0, evt.w, evt.h));
                break;
            }

            case MOUSE_DOWN:
                break;

            case MOUSE_UP:                
                break;

            case MOUSE_DRAG:
            case MOUSE_MOVE:
                break;            

            case WINDOW_CLOSE:
            case KEY_PRESS:
                running=false;
                break;
            default:
                break;
            }
        } else {
            usleep(1000);
        }

        // Check for time elapsed before re-rendering to limit framerate
        gettimeofday(&ctime, NULL);
        double etime = (double)(ctime.tv_sec-ltime.tv_sec) + (double)(ctime.tv_usec - ltime.tv_usec)/1e6;

        if (etime >= 1.0/60) {
            fps = 1.0/etime;
            update_balls(balls, nball, etime, width, height);
            redraw_screen(&pb, balls, nball, fps);
            ltime = ctime;
        }
        
        xwin_composite(&win, &pb);
    }
}
