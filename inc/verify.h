#ifndef VERIFY_H__
#define VERIFY_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


#define verify(condition, msg)                                  \
    _verify(__FILE__, __LINE__, condition, #condition, msg)

static inline void _verify(const char* file, unsigned line, bool condition, const char* cond_str, const char *msg) {
    if (!condition) {        
        fprintf(stderr, "%s:%u -- condition \"%s\" failed: %s\n", file, line, cond_str, msg);
        exit(EXIT_FAILURE);
    }
}

#endif//VERIFY_H__

