/*******************************************************************************
 * Copyright (c) 2012 Sean McAllister
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/

#ifndef GLX_H_
#define GLX_H_

#include <stdlib.h>

#include <GL/glu.h>
#include <GL/glx.h>

#include <xwin.h>
#include <verify.h>


// Represents a GLX context
typedef struct {
    xwin_t      *xwin;
    GLXContext   ctx;
    XVisualInfo *visual;
} glx_ctx_t;


// Creates a new GLX context via X server connection held in xwin
static inline glx_ctx_t glx_create(xwin_t *xwin) {
    int err_base, evt_base;
    verify(glXQueryExtension(xwin->dpy, &err_base, &evt_base),
           "GLX Extension not supported on target display");

    int attrs[] = {
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT | GLX_PIXMAP_BIT,
        GLX_DOUBLEBUFFER,  True,
        GLX_X_RENDERABLE,  True,
        GLX_RED_SIZE,      4,
        GLX_GREEN_SIZE,    4,
        GLX_BLUE_SIZE,     4,
        GLX_DEPTH_SIZE,    16,
        None
    };

    int num;
    GLXFBConfig *fb_configs = glXChooseFBConfig(xwin->dpy, xwin->screen, attrs, &num);
    verify(fb_configs  != NULL, "No suitable GLX framebuffer config found");

    XVisualInfo *visual = glXGetVisualFromFBConfig(xwin->dpy, fb_configs[0]);
    verify(visual      != NULL, "No suitable GLX visual found");

    glx_ctx_t glx_ctx;
    glx_ctx.xwin        = xwin;
    glx_ctx.visual      = visual;
    glx_ctx.ctx         = glXCreateContext(xwin->dpy, glx_ctx.visual, 0, GL_TRUE);
    verify(glx_ctx.ctx != NULL, "Couldn't create GLX context");

    return glx_ctx;
}


// Make GLX context current for rendering to given window
static inline int glx_make_current(xwin_t *xwin, glx_ctx_t *glx_ctx) {
    return glXMakeCurrent(xwin->dpy, xwin->window, glx_ctx->ctx);
}


// Swap back and front buffers
static inline void glx_swap(xwin_t *xwin) {
    glXSwapBuffers(xwin->dpy, xwin->window);
}


static inline void glx_free(glx_ctx_t *ctx) {
    // Destroy GLX context
    glXDestroyContext(ctx->xwin->dpy, ctx->ctx);
    ctx->ctx = NULL;
}

#endif // GLX_HH_
