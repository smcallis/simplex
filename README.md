# SimpleX Overview

SimpleX is an extremely simple interface to X11 that is designed to enable creation of custom visualizations with minimal dependencies.
This library was wrought from the desire to create customized visualizations in a laboratory setting, where source code is
frequently passed around between users wishing to use it.  In these circumstances, it's user friendly to have as few
external dependencies as possible to minimize the amount of external package searching and configuration required to build tools,
especially in the case of an environment where root isn't readily available or is cumbersome to obtain for package installation.

SimpleX is built to run in as wide a range of conditions as possible.  In particular, it consists of a single header file,
"simplex.h", which is written in ANSI-C99 (in actuality it's both C and C++ as it's written in the common subset of both).
The only requirement for compilation is the availability of the X11 headers at compile time and X11 libraries at runtime.

The library is mainly designed for pushing pixels, which is the core of what you want when designing custom visualizations
for data.  SimpleX provides the ability to draw pixels in a single format (32-bit RGBA) and then blit them to an X11 screen.
From this you can create surprisingly sophisticated GUIs and animation in a straightforward manner.


## What SimpleX gives you:
* Creation and manipulation of X11 windows via the xwin_t struct and xwin_* family of functions
* A common pixel buffer object via the pixbuf_t struct that allows drawing pixels in a common format (32-bit RGBA)
* Alpha-blended drawing routines including:
    * Lines (via fixed-point Wu's algorithm)
    * Circles (filled and hollow, and optionally only certain quadrants)
    * Rectangles (filled and hollow, convenience wrapper around line routines)
    * Nice default anti-aliased ASCII font rendering (based on Inconsolata font at 14pt)
* Event loop wrapping X11 events into nice common wrapper (event_t)


## What SimpleX doesn't give you:
* Any notion of widget or GUI toolkit, other than the window and pixbuffer mentioned above
* Full unicode font rendering (though it would be easy to add on top using FreeType)


## Principles

There are two primary entities of interest exposed by the library: windows and pixbufs. Windows are an abstraction of
an X11 window that simplify the X11 interface and have an associated event queue.  Pixbufs provide a simple common
drawing abstraction that always represents pixels as 8-bit RGBA.  Pixbufs may themselves be transparent, and they're
arranged hierarchically.  A pixbuf can be added as a child of another pixbuf and positioned relatively to its parent.
When the pixbuf_redraw function is called, the dirty region is recorded, and xwin_composite can be used at the bottom
of the event loop to blend all the dirty regions to a backing buffer and flush the changes to the screen.
This makes it very easy to update particular regions of the screen and localizes where dirty geometry has to be computed.
When xwin_composite is called, any dirty regions are blended down into a blend buffer owned by the window, and the resulting
pixel values are color-depth converted, gamma corrected and blitted to the screen.  Whenever an Expose event occurs, the window is
redrawn from the current contents of the blend buffer, providing native backing store support.


          +----------------------+
          |                      |
     +----+----------------+     |               +-----------------------+
     |    |                |     |               |                       |
     |    | +------+       |     |  Blend        |                       |  
     |    | | PB 3 |       |     |  ---------->  |                       |  
     |    | +------+       |     |               |                       |  
     |    |                |     |               |                       |
     |    | PB1            |     |               |                       |
     |    +----------------+-----+               |                       |
     | PB2                 |                     | Blend Buffer          |
     +---------------------+                     +-----------------------+

                                                 Color Depth   |
                                                 Gamma Correct |
                                                               |
                                                               v

          +----------------------+               +-----------------------+
          |                      |               |                       |
          |                      |  Expose       |                       |
          |                      |  Events       |                       |
          |                      |  <----------  |                       |
          |                      |               |                       |
          |                      |               |                       |
          |                      |               |                       |
          | X11 Window           |               | X11 Pixmap            |
          +----------------------+               +-----------------------+




## Usage

A very simple usage example (test.c):

    :::c
    #include <simplex.h>                                                            
                                                                                    
    int main() {                                                                    
        /* Get bounding box around message */                                       
        const char* msg = "Hello world!";                                           
        uint16_t msgw, msgh;                                                        
        text_bbox(msg, &msgw, &msgh);                                               
                                                                                    
        size_t width  = 800;                                                        
        size_t height = 600;                                                        
                                                                                    
        xwin_t xwin;                                                                
        xwin_init (&xwin, 0, width, height, 200, 200);                              
        xwin_title(&xwin, "SimpleX demonstration");                                 
                                                                                    
        pixbuf_t pb;                                                                
        pixbuf_init  (&pb, width, height);                                          
        pixbuf_redraw(&pb, rectangle(0, 0, width, height));                         
                                                                                    
        bool running = true;                                                        
        while (running) {                                                           
            event_t evt;                                                            
            if (xwin_event(&xwin, &evt)) {                                          
                switch(evt.type) {                                                  
                    case WINDOW_RESIZE: {                                           
                        width  = evt.w;                                             
                        height = evt.h;                                             
                        pixbuf_resize (&pb, width, height);                         
                                                                                    
                        pixel_t white = rgb_color(0xFFFFFF);                        
                        size_t  xx    =  width/2 - msgw/2;                          
                        size_t  yy    = height/2 - msgh/2;                          
                        draw_text(&pb, white, xx, yy, msg);                         
                        pixbuf_redraw(&pb, rectangle(0, 0, width, height));         
                        break;                                                      
                    }                                                               
                    case WINDOW_CLOSE:                                              
                    case KEY_PRESS:                                                 
                        running = false; break;                                     
                    default:                                                        
                        break;                                                      
                }                                                                   
            }                                                                       
                                                                                    
            xwin_composite(&xwin, &pb);                                             
        }                                                                           
                                                                                    
        return 0;                                                                   
    }                           

Compile with:
    gcc -std=c99 -pedantic -Iinc test.c -o test -lX11 -lm


## Events

Events are encapsulated by the event_t structure and the evt_type_t enum:

    // Event types
    typedef enum {
        WINDOW_RESIZE, WINDOW_CLOSE,

        MOUSE_ENTER,   MOUSE_LEAVE,  MOUSE_MOVE,
        MOUSE_DOWN,    MOUSE_DRAG,   MOUSE_UP,

        KEY_PRESS
    } evt_type_t;


    // Event structure, holds event type and information about it
    typedef struct {
        evt_type_t type;
        int c,m,x,y,w,h;

        unsigned mod; // Any modifiers for a key press    
        KeySym   sym; // Raw KeySym
    } event_t;


A brief description of the currently supported events:

    WINDOW_RESIZE  Window was resized, new size is in evt.w and evt.h 
    WINDOW_CLOSE   Window was closed by the user, time to cleanup      
    MOUSE_ENTER    Mouse entered window, entry coordinates are evt.x/y 
    MOUSE_LEAVE    Mouse left window, exit coordinates are evt.x/y     
    MOUSE_MOVE     Mouse coordinates changed without button held down.  Coordinates are in evt.x/y
    MOUSE_DOWN     Mouse button was pressed, coordinates are evt.x/y, button is in evt.m  
    MOUSE_UP       Mouse button was released, coordinates are evt.x/y, button is in evt.m 
    MOUSE_DRAG     Mouse was moved with button down, coordinates are evt.x/y, button is in evt.m, dimensions of drag are evt.w/h 
    KEY_PRESS      User pressed keyboard key, ASCII key code is in evt.c, mouse coordinates at time of press are in evt.x/y

### Key Press Events

Key Press events are a little more complex than other events, because things like modifier keys, and non-ASCII keys must be 
handled.  The procedure for this is simple, when a KeyPress event is received, SimpleX tries to convert the pressed key code into
an ASCII character.  Any modifiers keys that were pressed during the KeyPress are masked into evt.mod, and can be queried
using the constants ShiftMask, LockMask, and ControlMask (alt is usually masked using Mod1Mask).  If the key can't be converted
to an ASCII character, then the evt.c field is set to zero, and the key sym can be used directly via the evt.sym field.  Standard
X11 KeySym values as defined in X11/keysymdef.h can be used to query these values.


## Index of functions
### pixel/color functions
    pixel_t  rgba_pixel         - build pixel_t out of individual RGBA color fields 
    pixel_t  rgb_pixel          - build pixel_t out of individual RGB color fields 
    pixel_t  rgba_color         - build pixel_t out of 32-bit RGBA integer
    pixel_t  rgb_color          - build pixel_t out of 32-bit RGB integer
    pixel_t  pixel_set_alpha    - set un-premultiplied pixel values alpha value
    pixel_t  pixel_unset_alpha  - undo pre-multiplication an return un-premultiplied pixel value
    pixel_t  blend_pixel        - alpha blend two pixel values together and return result.

### coordinates, rectangles and points
    point_t  point              - create new point with given values
    rect_t   rectangle          - create new rectangle from top/left coordinate and width/height
    rect_t   rectanglep         - create new rectangle from two points 
    bool     rect_empty         - return true if rectangle is empty
    coord_t  rect_width         - return width of rectangle
    coord_t  rect_height        - return height of rectangle 
    rect_t   rect_shrink        - shrink a rectangle by a given amount
    rect_t   rect_grow          - grow a rectangle by a given amount
    rect_t   rect_isect         - return intersection between two rectangles 
    rect_t   rect_bbox          - return bounding rectangle of two rectangles
    bool     rect_equal         - return true if two rectangle are equal
    bool     in_range           - return true if a coordinate is between two bounds in the half open [ll,hh) sense.
    bool     point_in_rect      - return true if a point is contained in a rectangle
    point_t  map_point          - map a point given in coordinates specified by src to coordinates specified by dst
    rect_t   map_rect           - map a rectangle given in coordinates specified by src to coordinates specified by dst
    point_t  translate_point    - translate a point from src origin to dst origin

### pixbuf functions
    int      pixbuf_init        - initialize pixbuffer with given dimensions
    int      pixbuf_resize      - resize pixbuffer to given dimensions, uses calloc to zero new memory, resets clip rectangle
    void     pixbuf_free        - free memory associated with pixbuffer
    bool     pixbuf_set_blend   - enable/disable alpha blending when drawing into pixbuffer
    rect_t   pixbuf_clip        - set clipping rectangle for pixbuffer
    void     pixbuf_unclip      - reset clipping rectangle for pixbuffer to full size
    uint32_t pixbuf_clip_x      - clip x coordinate using pixbuffer clipping rectangle 
    uint32_t pixbuf_clip_y      - clip y coordinate using pixbuffer clipping rectangle 
    void     pixbuf_clear_rect  - clear rectangular region of pixbuffer to given color
    void     pixbuf_clear       - clear entire pixbuffer to given color
    void     pixbuf_redraw      - mark given region of pixbuffer as dirty
    void     pixbuf_add_child   - add pixbuffer as child to another pixbuffer
    void     pixbuf_clear_dirty - unconditionally clear dirty list for pixbuffer
    rect_t*  pixbuf_composite   - composite a pixbuffer and its children into a target pixbuffer, returns list of dirty regions to blit to screen

### X11 functions
    int      xwin_init          - initialize new X11 window
    void     xwin_free          - destroy X11 window
    void     xwin_title         - set title text of window 
    void     xwin_resize        - set window dimensions 
    void     xwin_move          - set window position relative to parent
    bool     xwin_event         - poll for a new event, return true if there is one and return it through paramter
    void     xwin_gamma         - set gamma correction value for window (default is 0.7)
    void     xwin_composite     - pixbuf_composite a pixbuf into the windows blend buffer and blit to screen
    void     xwin_no_cursor     - disable X11 provided cursor when inside our window

### drawing functions
    void     draw_pixel         - draw a pixel of a given color to the pixbuffer, no clipping is performed (fast but unsafe)
    void     draw_pixel_clip    - draw a pixel of a given color to the pixbuffer, clipping is performed 
    void     draw_vline_pattern - draw vertical line to the pixbuffer with stippling pattern 
    void     draw_vline         - draw solid vertical line to the pixbuffer
    void     draw_hline_pattern - draw horizontal line to the pixbuffer with stippling pattern 
    void     draw_hline         - draw solid horizontal line to the pixbuffer
    void     draw_linef         - draw sub-pixel line to screen using fast-prefiltering algorithm 
    void     draw_line          - draw line to screen using Wu's antialiasing algorithm 
    void     draw_rect          - draw rectangular outline of given thickness to pixbuffer
    void     draw_rect_fill     - draw filled rectangle to screen 
    void     draw_circ          - draw circlular outline to screen using Wu's circle algorithm
    void     draw_circ_fill     - draw filled circle to scren using Wu's circle algorithm
    void     font_bboxn         - return dimensions of text rendered using given font with given length
    void     font_bbox          - return dimensions of text rendered using given font and strlen
    int      font_maxy          - return maximum y offset of given font
    int      font_miny          - return minimum y offset of given font 
    void     text_bboxn         - return dimensions of text rendered using default font and given length
    void     text_bbox          - return dimensions of text rendered using default font and strlen
    void     draw_fontn         - draw text with given font and given string length
    void     draw_font          - draw text with given font and strlen 
    void     draw_textn         - draw text with default font and given string length 
    void     draw_text          - draw text with default font and strlen
