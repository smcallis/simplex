override CC     := g++
override CCOPTS := -ansi -pedantic -Wall -Wextra -Iinc/ -O3 $(CCOPTS)
override LDOPTS :=

EXAMPLES :=         \
	bin/example \
	bin/lines   \
	bin/bouncy  \

all: $(EXAMPLES)

bin/%: src/%.cc inc inc/simplex.h
	$(CC) $(CCOPTS) $< -lX11 -lm -o $@ 

# Cleanup and rebuild targets
clean: 
	@rm -f bin/*

remake: clean all
